package ru.inshakov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.model.IProjectGraphRepository;
import ru.inshakov.tm.api.service.IConnectionService;
import ru.inshakov.tm.api.service.model.IProjectGraphService;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyIndexException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.exception.system.IndexIncorrectException;
import ru.inshakov.tm.model.ProjectGraph;
import ru.inshakov.tm.model.UserGraph;
import ru.inshakov.tm.repository.model.ProjectGraphRepository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public final class ProjectGraphService extends AbstractGraphService<ProjectGraph> implements IProjectGraphService {

    @NotNull
    public ProjectGraphService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectGraph> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectGraphRepository repository = new ProjectGraphRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<ProjectGraph> collection) {
        if (collection == null) return;
        for (ProjectGraph item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    public ProjectGraph add(@Nullable final ProjectGraph entity) {
        if (entity == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository repository = new ProjectGraphRepository(entityManager);
            repository.add(entity);
            entityManager.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    private void update(@NotNull final ProjectGraph entity, @NotNull EntityManager entityManager) {
        @NotNull final IProjectGraphRepository repository = new ProjectGraphRepository(entityManager);
        repository.update(entity);
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectGraph findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectGraphRepository repository = new ProjectGraphRepository(entityManager);
            return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository repository = new ProjectGraphRepository(entityManager);
            @NotNull final List<ProjectGraph> projectGraphs = repository.findAll();
            for (ProjectGraph t :
                    projectGraphs) {
                repository.remove(t);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository repository = new ProjectGraphRepository(entityManager);
            @Nullable final ProjectGraph projectGraph = repository.getReference(optionalId.orElseThrow(EmptyIdException::new));
            if (projectGraph == null) return;
            repository.remove(projectGraph);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final ProjectGraph entity) {
        if (entity == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository repository = new ProjectGraphRepository(entityManager);
            @Nullable final ProjectGraph projectGraph = repository.getReference(entity.getId());
            if (projectGraph == null) return;
            repository.remove(projectGraph);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }


    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph findByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            if (index > projectRepository.findAllByUserId(userId).size() - 1) throw new IndexIncorrectException();
            return projectRepository.findByIndex(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectGraph findByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            return projectRepository.findByName(userId, name);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            if (index > projectRepository.findAllByUserId(userId).size() - 1) throw new IndexIncorrectException();
            @Nullable final ProjectGraph projectGraph = projectRepository.findByIndex(userId, index);
            if (projectGraph == null) return;
            projectRepository.remove(projectGraph);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            @Nullable final ProjectGraph projectGraph = projectRepository.findByName(userId, name);
            if (projectGraph == null) return;
            projectRepository.remove(projectGraph);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph updateById
            (@NotNull final String userId, @Nullable final String id,
             @Nullable final String name, @Nullable final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            @NotNull final ProjectGraph projectGraph = Optional.ofNullable(projectRepository.findByIdUserId(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            projectGraph.setName(name);
            projectGraph.setDescription(description);
            update(projectGraph, entityManager);
            entityManager.getTransaction().commit();
            return projectGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph updateByIndex
            (@NotNull final String userId, @Nullable final Integer index,
             @Nullable final String name, @Nullable final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            @NotNull final ProjectGraph projectGraph = Optional.ofNullable(projectRepository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new);
            projectGraph.setName(name);
            projectGraph.setDescription(description);
            update(projectGraph, entityManager);
            entityManager.getTransaction().commit();
            return projectGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph startById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            @NotNull final ProjectGraph projectGraph = Optional.ofNullable(projectRepository.findByIdUserId(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            projectGraph.setStatus(Status.IN_PROGRESS);
            projectGraph.setStartDate(new Date());
            update(projectGraph, entityManager);
            entityManager.getTransaction().commit();
            return projectGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph startByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            @NotNull final ProjectGraph projectGraph = Optional.ofNullable(projectRepository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new);
            projectGraph.setStatus(Status.IN_PROGRESS);
            projectGraph.setStartDate(new Date());
            update(projectGraph, entityManager);
            entityManager.getTransaction().commit();
            return projectGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph startByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            @NotNull final ProjectGraph projectGraph = Optional.ofNullable(projectRepository.findByName(userId, name))
                    .orElseThrow(ProjectNotFoundException::new);
            projectGraph.setStatus(Status.IN_PROGRESS);
            projectGraph.setStartDate(new Date());
            update(projectGraph, entityManager);
            entityManager.getTransaction().commit();
            return projectGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph finishById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            @NotNull final ProjectGraph projectGraph = Optional.ofNullable(projectRepository.findByIdUserId(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            projectGraph.setStatus(Status.COMPLETED);
            projectGraph.setFinishDate(new Date());
            update(projectGraph, entityManager);
            entityManager.getTransaction().commit();
            return projectGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph finishByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            @NotNull final ProjectGraph projectGraph = Optional.ofNullable(projectRepository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new);
            projectGraph.setStatus(Status.COMPLETED);
            projectGraph.setFinishDate(new Date());
            update(projectGraph, entityManager);
            entityManager.getTransaction().commit();
            return projectGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectGraph finishByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository projectRepository = new ProjectGraphRepository(entityManager);
            @NotNull final ProjectGraph projectGraph = Optional.ofNullable(projectRepository.findByName(userId, name))
                    .orElseThrow(ProjectNotFoundException::new);
            projectGraph.setStatus(Status.COMPLETED);
            projectGraph.setFinishDate(new Date());
            update(projectGraph, entityManager);
            entityManager.getTransaction().commit();
            return projectGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    public ProjectGraph add(@NotNull UserGraph userGraph, @Nullable String name, @Nullable String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectGraph projectGraph = new ProjectGraph(name, description);
        projectGraph.setUser(userGraph);
        return add(projectGraph);
    }

    @NotNull
    @Override
    public List<ProjectGraph> findAll(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectGraphRepository repository = new ProjectGraphRepository(entityManager);
            return repository.findAllByUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@NotNull final UserGraph userGraph, @Nullable final Collection<ProjectGraph> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (ProjectGraph item : collection) {
            item.setUser(userGraph);
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectGraph add(@NotNull final UserGraph userGraph, @Nullable final ProjectGraph entity) {
        if (entity == null) return null;
        entity.setUser(userGraph);
        @Nullable final ProjectGraph entityResult = add(entity);
        return entityResult;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectGraph findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectGraphRepository repository = new ProjectGraphRepository(entityManager);
            return repository.findByIdUserId(userId, optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository repository = new ProjectGraphRepository(entityManager);
            @NotNull final List<ProjectGraph> projectGraphs = repository.findAllByUserId(userId);
            for (ProjectGraph t :
                    projectGraphs) {
                repository.remove(t);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository repository = new ProjectGraphRepository(entityManager);
            @Nullable final ProjectGraph projectGraph = repository.findByIdUserId(
                    userId,
                    optionalId.orElseThrow(EmptyIdException::new)
            );
            if (projectGraph == null) return;
            repository.remove(projectGraph);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @Nullable final ProjectGraph entity) {
        if (entity == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectGraphRepository repository = new ProjectGraphRepository(entityManager);
            @Nullable final ProjectGraph projectGraph = repository.findByIdUserId(userId, entity.getId());
            if (projectGraph == null) return;
            repository.remove(projectGraph);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
