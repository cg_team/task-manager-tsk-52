package ru.inshakov.tm.exception.entity;

public class AccessForbiddenException extends RuntimeException {
    public AccessForbiddenException() {
        super("Error! Access forbidden...");
    }
}