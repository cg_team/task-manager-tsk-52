package ru.inshakov.tm.exception.entity;

public final class TaskNotFoundException extends RuntimeException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}
