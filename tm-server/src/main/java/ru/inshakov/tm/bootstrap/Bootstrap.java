package ru.inshakov.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.api.service.*;
import ru.inshakov.tm.api.service.dto.IProjectService;
import ru.inshakov.tm.api.service.dto.IProjectTaskService;
import ru.inshakov.tm.api.service.dto.ISessionService;
import ru.inshakov.tm.api.service.dto.ITaskService;
import ru.inshakov.tm.api.service.dto.IUserService;
import ru.inshakov.tm.api.service.model.IProjectGraphService;
import ru.inshakov.tm.api.service.model.ISessionGraphService;
import ru.inshakov.tm.api.service.model.ITaskGraphService;
import ru.inshakov.tm.api.service.model.IUserGraphService;
import ru.inshakov.tm.component.MessageExecutor;
import ru.inshakov.tm.endpoint.*;
import ru.inshakov.tm.service.*;
import ru.inshakov.tm.service.dto.*;
import ru.inshakov.tm.service.model.ProjectGraphService;
import ru.inshakov.tm.service.model.SessionGraphService;
import ru.inshakov.tm.service.model.TaskGraphService;
import ru.inshakov.tm.service.model.UserGraphService;
import ru.inshakov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@Getter
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private static final MessageExecutor messageExecutor = new MessageExecutor();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskService taskDtoService = new TaskService(connectionService);

    @NotNull
    private final IProjectService projectDtoService = new ProjectService(connectionService);

    @NotNull
    private final IProjectTaskService projectTaskDtoService = new ProjectTaskService(connectionService);

    @NotNull
    private final IUserService userDtoService = new UserService(connectionService, propertyService);

    @NotNull
    private final ISessionService sessionDtoService = new SessionService(connectionService, userDtoService, propertyService);

    @NotNull
    private final ITaskGraphService taskService = new TaskGraphService(connectionService);

    @NotNull
    private final IProjectGraphService projectService = new ProjectGraphService(connectionService);

    @NotNull
    private final IUserGraphService userService = new UserGraphService(connectionService, propertyService);

    @NotNull
    private final ISessionGraphService sessionService = new SessionGraphService(connectionService, userService, propertyService);

    @NotNull
    private final DataService dataService = new DataService(userDtoService, taskDtoService, projectDtoService, sessionDtoService);

    @NotNull
    private final ILoggerService logService = new LoggerService();


    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this, projectDtoService, projectTaskDtoService, projectService);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(this, sessionDtoService, userDtoService, sessionService);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this, taskDtoService, projectTaskDtoService, taskService);

    @NotNull
    private final AdminEndpoint adminEndpoint = new AdminEndpoint(this, userDtoService, userService, sessionDtoService);

    @NotNull
    private final DataEndpoint dataEndpoint = new DataEndpoint(this, dataService);

    public void init() {
        initPID();
        initEndpoints();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    public void initJMSBroker() {
        BasicConfigurator.configure();
        @NotNull final BrokerService brokerService = new BrokerService();
        brokerService.addConnector("tcp://localhost:61616");
        brokerService.start();
    }

    private void registry(final Object endpoint) {
        if (endpoint == null) return;
        final String host = propertyService.getServerHost();
        final String port = propertyService.getServerPort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl,endpoint);
    }

    @SneakyThrows
    private void initEndpoints(){
        @NotNull final Reflections reflections = new Reflections("ru.inshakov.tm.endpoint");
        @NotNull final Set<Class<? extends AbstractEndpoint>> classes =
                reflections.getSubTypesOf(AbstractEndpoint.class);
        for (@NotNull final Class<? extends AbstractEndpoint> clazz:classes){
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    public static void sendMessage(@Nullable final Object record,
                                   @NotNull final String type) {
        messageExecutor.sendMessage(record, type);
    }

}
