package ru.inshakov.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.UUID;

@Getter
@XmlRootElement
@AllArgsConstructor
public class Logger implements Serializable {

    private final String id = UUID.randomUUID().toString();

    private final String entity;

    private final String formattedDate;

    private final String className;

    private final String type;

}