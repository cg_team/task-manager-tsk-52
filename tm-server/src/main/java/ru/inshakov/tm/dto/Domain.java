package ru.inshakov.tm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@JsonRootName("domain")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Domain implements Serializable {

    @Nullable
    private String date = new Date().toString();

    @Nullable
    @JsonProperty("userGraph")
    @JacksonXmlElementWrapper(localName = "users")
    @XmlElement(name = "userGraph")
    @XmlElementWrapper(name = "users")
    private List<User> users;

    @Nullable
    @JsonProperty("projectGraph")
    @JacksonXmlElementWrapper(localName = "projectGraphs")
    @XmlElement(name = "projectGraph")
    @XmlElementWrapper(name = "projectGraphs")
    private List<Project> projects;

    @Nullable
    @JsonProperty("task")
    @JacksonXmlElementWrapper(localName = "taskGraphs")
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "taskGraphs")
    private List<Task> tasks;

}
