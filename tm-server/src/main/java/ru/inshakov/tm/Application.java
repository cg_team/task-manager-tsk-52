package ru.inshakov.tm;

import ru.inshakov.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
        bootstrap.initJMSBroker();
    }

}