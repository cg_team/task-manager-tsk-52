package ru.inshakov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.exception.system.IndexIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @Nullable
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        @Nullable final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new IndexIncorrectException(value);
        }
    }
}
