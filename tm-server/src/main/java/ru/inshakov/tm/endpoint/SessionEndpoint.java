package ru.inshakov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.inshakov.tm.api.service.ServiceLocator;
import ru.inshakov.tm.api.service.dto.ISessionService;
import ru.inshakov.tm.api.service.dto.IUserService;
import ru.inshakov.tm.api.service.model.ISessionGraphService;
import ru.inshakov.tm.dto.Session;
import ru.inshakov.tm.dto.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public final class SessionEndpoint extends AbstractEndpoint {

    private ISessionService sessionDtoService;

    private IUserService userService;

    private ISessionGraphService sessionService;

    public SessionEndpoint(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final ISessionService sessionDtoService,
            @NotNull final IUserService userService,
            @NotNull final ISessionGraphService sessionService
    ) {
        super(serviceLocator);
        this.sessionDtoService = sessionDtoService;
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @WebMethod
    public Session open(
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password
    ) {

        Session session = sessionDtoService.open(login, password);
        return session;
    }

    @WebMethod
    public void close(@WebParam(name = "session") final Session session) {
        serviceLocator.getSessionDtoService().validate(session);
        sessionDtoService.close(session);
    }

    @WebMethod
    public Session register(
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password,
            @WebParam(name = "email") final String email
    ) {
        userService.add(login, password, email);
        return sessionDtoService.open(login, password);
    }

    @WebMethod
    public User setPassword(
            @WebParam(name = "session") final Session session, @WebParam(name = "password") final String password
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return userService.setPassword(session.getUserId(), password);
    }

    @WebMethod
    public User updateUser(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "firstName") final String firstName,
            @WebParam(name = "lastName") final String lastName,
            @WebParam(name = "middleName") final String middleName
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return userService.updateUser(session.getUserId(), firstName, lastName, middleName);
    }

}
