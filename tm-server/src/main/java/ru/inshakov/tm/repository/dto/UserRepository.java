package ru.inshakov.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.dto.IUserRepository;
import ru.inshakov.tm.dto.User;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    public List<User> findAll() {
        return entityManager.createQuery("SELECT e FROM User e", User.class).getResultList();
    }

    public User findById(@Nullable final String id) {
        return entityManager.find(User.class, id);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM User e")
                .executeUpdate();
    }

    public void removeById(@Nullable final String id) {
        User reference = entityManager.getReference(User.class, id);
        entityManager.remove(reference);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM User e WHERE e.login = :login", User.class)
                        .setHint(QueryHints.HINT_CACHEABLE, true)
                        .setParameter("login", login)
                        .setMaxResults(1)
        );
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM User e WHERE e.email = :email", User.class)
                        .setHint(QueryHints.HINT_CACHEABLE, true)
                        .setParameter("email", email)
                        .setMaxResults(1)
        );
    }

    @Override
    public void removeUserByLogin(@Nullable final String login) {
        entityManager
                .createQuery("DELETE FROM User e WHERE e.login = :login")
                .setParameter(login, login)
                .executeUpdate();
    }

}