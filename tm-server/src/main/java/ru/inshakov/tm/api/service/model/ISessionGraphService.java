package ru.inshakov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IGraphService;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.model.SessionGraph;
import ru.inshakov.tm.model.UserGraph;

import java.util.List;

public interface ISessionGraphService extends IGraphService<SessionGraph> {

    SessionGraph open(@Nullable String login, @Nullable String password);

    UserGraph checkDataAccess(@Nullable String login, @Nullable String password);

    void validate(@NotNull SessionGraph sessionGraph, Role role);

    void validate(@Nullable SessionGraph sessionGraph);

    SessionGraph sign(@Nullable SessionGraph sessionGraph);

    void close(@Nullable SessionGraph sessionGraph);

    void closeAllByUserId(@Nullable String userId);

    @Nullable List<SessionGraph> findAllByUserId(@Nullable String userId);
}
