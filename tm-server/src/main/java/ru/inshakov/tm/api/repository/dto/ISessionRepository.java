package ru.inshakov.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.dto.Session;

import java.util.List;

public interface ISessionRepository {

    List<Session> findAllByUserId(@Nullable String userId);

    void removeByUserId(@Nullable String userId);

    void add(final Session session);

    void update(final Session session);

    List<Session> findAll();

    Session findById(final String id);

    void clear();

    void removeById(final String id);

}
