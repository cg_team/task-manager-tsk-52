package ru.inshakov.tm.api.repository.model;

import ru.inshakov.tm.api.IGraphRepository;
import ru.inshakov.tm.model.TaskGraph;

import java.util.List;

public interface ITaskGraphRepository extends IGraphRepository<TaskGraph> {

    List<TaskGraph> findAllTaskByProjectId(final String userId, final String projectId);

    void removeAllTaskByProjectId(final String userId, final String projectId);

    void bindTaskToProjectById(final String userId, final String taskId, final String projectId);

    void unbindTaskById(final String userId, final String id);

    void update(final TaskGraph taskGraph);

    TaskGraph findByIdUserId(final String userId, final String id);

    void clearByUserId(final String userId);

    void removeByIdUserId(final String userId, final String id);

    List<TaskGraph> findAllByUserId(final String userId);

    TaskGraph findByName(final String userId, final String name);

    TaskGraph findByIndex(final String userId, final int index);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final int index);


}
