package ru.inshakov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.dto.Logger;

public interface IMessageService {

    void sendMessage(@NotNull Logger entity);

    @SneakyThrows
    Logger prepareMessage(@Nullable Object record,
                             @NotNull String type);
}
