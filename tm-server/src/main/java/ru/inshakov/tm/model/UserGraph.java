package ru.inshakov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.listener.EntityListener;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "tm_user")
@XmlAccessorType(XmlAccessType.FIELD)
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserGraph extends AbstractGraphEntity {

    @NotNull
    @Column(unique = true)
    private String login;

    @NotNull
    @Column(name = "password_hash")
    private String passwordHash;

    @Nullable
    @Column(unique = true)
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

    @Column
    private boolean locked = false;

    @Nullable
    @OneToMany(mappedBy = "userGraph", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProjectGraph> projectGraphs = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "userGraph", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TaskGraph> taskGraphs = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "userGraph", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<SessionGraph> sessionGraphs = new ArrayList<>();

}