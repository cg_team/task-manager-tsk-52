package ru.inshakov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;
import java.util.UUID;

@Setter
@Getter
@MappedSuperclass
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class AbstractGraphEntity implements Serializable {

    @Id
    @NotNull
    protected String id = UUID.randomUUID().toString();

}