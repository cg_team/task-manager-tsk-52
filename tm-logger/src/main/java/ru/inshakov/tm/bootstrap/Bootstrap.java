package ru.inshakov.tm.bootstrap;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.inshakov.tm.api.service.IReceiverService;
import ru.inshakov.tm.listener.LogMessageListener;
import ru.inshakov.tm.service.ReceiverService;

import static ru.inshakov.tm.constant.ActiveMQConst.URL;

public final class Bootstrap {

    public void start() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(URL);
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        receiverService.receive(new LogMessageListener());
    }

}
