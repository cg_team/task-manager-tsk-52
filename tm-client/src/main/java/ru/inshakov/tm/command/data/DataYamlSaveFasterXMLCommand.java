package ru.inshakov.tm.command.data;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;

public class DataYamlSaveFasterXMLCommand extends AbstractCommand {

    @Nullable

    public String name() {
        return "data-save-yaml";
    }

    @Nullable

    public String arg() {
        return null;
    }

    @Nullable

    public String description() {
        return "Save data to YAML by FasterXML.";
    }

    public void execute() {
        serviceLocator.getDataEndpoint().saveDataYaml(getSession());
    }

}