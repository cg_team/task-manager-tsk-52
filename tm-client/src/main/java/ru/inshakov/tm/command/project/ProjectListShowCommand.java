package ru.inshakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.ProjectAbstractCommand;
import ru.inshakov.tm.endpoint.Project;

import java.util.List;

public class ProjectListShowCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "projectGraph-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all projectGraphs.";
    }

    @Override
    public void execute() {
        System.out.println("Enter sort");
        @Nullable List<Project> projects = serviceLocator.getProjectEndpoint().findProjectAll(getSession());
        int index = 1;
        for (@NotNull final Project project : projects) {
            System.out.println(index + ". " + project.toString());
            index++;
        }
    }
}
