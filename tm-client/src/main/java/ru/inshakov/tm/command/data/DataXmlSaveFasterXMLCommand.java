package ru.inshakov.tm.command.data;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;

public class DataXmlSaveFasterXMLCommand extends AbstractCommand {

    @Nullable

    public String name() {
        return "data-save-xml-f";
    }

    @Nullable

    public String arg() {
        return null;
    }

    @Nullable

    public String description() {
        return "Save data to XML by FasterXML.";
    }

    public void execute() {
        serviceLocator.getDataEndpoint().saveDataXml(getSession());
    }

}