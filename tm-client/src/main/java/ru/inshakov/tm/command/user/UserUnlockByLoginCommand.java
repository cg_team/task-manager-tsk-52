package ru.inshakov.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.util.TerminalUtil;

public class UserUnlockByLoginCommand extends AbstractCommand {

    @Override
    public String name() {
        return "userGraph-unlock-by-login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Unlock userGraph by login";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        @Nullable final String login = TerminalUtil.nextLine();
        serviceLocator.getAdminEndpoint().unlockByLogin(getSession(), login);
    }

}
