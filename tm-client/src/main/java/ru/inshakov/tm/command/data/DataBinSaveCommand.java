package ru.inshakov.tm.command.data;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;

public class DataBinSaveCommand extends AbstractCommand {

    @Nullable

    public String name() {
        return "data-save-bin";
    }

    @Nullable

    public String arg() {
        return null;
    }

    @Nullable

    public String description() {
        return "Save binary data";
    }

    public void execute() {
        serviceLocator.getDataEndpoint().saveDataBin(getSession());
    }

}