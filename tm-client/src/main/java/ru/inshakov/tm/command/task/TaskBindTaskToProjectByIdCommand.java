package ru.inshakov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.TaskAbstractCommand;
import ru.inshakov.tm.endpoint.Task;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.util.TerminalUtil;

public class TaskBindTaskToProjectByIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-bind-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Bind task to projectGraph.";
    }

    @Override
    public void execute() {
        System.out.println("Enter task id");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @Nullable final Task task = serviceLocator.getTaskEndpoint().findTaskById(getSession(), taskId);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter projectGraph id");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final Task taskUpdated = serviceLocator.getTaskEndpoint().bindTaskById(getSession(), taskId, projectId);
        if (taskUpdated == null) throw new TaskNotFoundException();
    }
}
